""" Сервер для тестового сайта. 
    Перед запуском установить:
        pip install fastapi
        pip install "uvicorn[standard]"
        pip install python-multipart

    Команда для запуска: 
        uvicorn server:app --reload --host '0.0.0.0' --port 80
    
    Чтобы сервер продолжил работать после закрытия терминала, запустить с помощью setsid.
    Чтобы перенаправить поток stderr stdout в файл нужно добавить: &> log 
    Итого можно запустить вот так: 
        setsid uvicorn server:app --reload --host '0.0.0.0' --port 80 &> log

    Чтобы следить за файлом log в реальном времени можно использовать команду "less", нажав после запуска "shift+F"
"""


from typing import Union
from fastapi import FastAPI, Form
from fastapi.responses import HTMLResponse

app = FastAPI()


item_storage = []


@app.get("/", response_class=HTMLResponse)
def read_root():
    return '''
    <html>
        <head>
        </head>
        <body>
            <h1>Привет</h1>
            <div>
                <a href="/items">Список</a>
            </div>
            <form action="/items" method="POST">
                <label for="name">Название элемента:</label><br>
                <input type="text" id="name" name="name">
                <input type="submit" value="отправить">
            </form>
        </body>
    </html>
    '''

@app.post("/items", response_class=HTMLResponse)
def create_item(name: str = Form(...)):
    item_storage.append(name)
    return F'''
    <html>
        <head>
        </head>
        <body>
            <h1>Привет</h1>
            <div>
                <a href="/">Главная</a>
                <a href="/items">Список</a>
            </div>
            <div>Элемент добавлен: {name}</div>
        </body>
    </html>
    '''


@app.get("/items/{item_id}")
def read_item(item_id: int):
    return {"item_id": item_id, "name": item_storage[item_id]}



@app.get("/items", response_class=HTMLResponse)
def get_items():
    items = ""
    for item in item_storage:
        items += F"<li>{item}</li>\n"

    return F'''
    <html>
        <head>
        </head>
        <body>
            <h1>Привет</h1>
            <div>
                <a href="/">Главная</a>
            </div>
            <ul>
                {items}
            </ul>
        </body>
    </html>
    '''


# scp - secure copy, утилита для копирования файлов на сервер по шифрованному каналу, тому же что использует ssh.
# синтаксис: scp файл_или_папка_которую_скопировать имя_пользователя_на_удаленном_компьютере@ip_адрес_или_домен:путь_куда_скопировать_на_удаленном_компьютере
# это самый базовый способ развертывания или загрузки ПО на сервер (буква D в CI/CD - deployment)
# Пример: 
#  scp server.py root@194.67.66.250:~/



# ssh - secure shell, утилита для доступа к командной строке удаленного компьютера по шифрованному каналу.
# синтаксис: ssh имя_пользователя_на_удаленном_компьютере@ip_адрес_или_домен
# Пример: 
#  ssh root@194.67.66.250


